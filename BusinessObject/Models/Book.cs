﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.Models
{
    public class Book
    {

        public int BookId { get; set; }

        public string? Title { get; set; }

        public string? Type { get; set; }

        public int PubId { get; set; }

        public double Price { get; set; }

        public string? Advance { get; set; }

        public double Royalty { get; set; }

        //Doanh so
        public int YtdSales { get; set; }

        public string? Notes { get; set; }

        public DateOnly PublishedDate { get; set; }


    }
}
